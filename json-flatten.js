const JSONFlatten = (obj) => {

  let flattened = obj;

  while(!isFlat(flattened)) {
    flattened = flattenOne(flattened);
  }

  return flattened;
}

const flattenOne = (obj) => {
  let flattened = {};

  for(let key in obj) {
    (isObject(obj[key])) ? flattenFromKeys(flattened, obj[key], key) : assignKeyValue(flattened, key, obj[key]);
  }

  return flattened;
};

const flattenFromKeys = (target, source, outerKey) => {
  for(let innerKey in source) {
    let newKey = outerKey + '.' + innerKey;
    assignKeyValue(target, newKey, source[innerKey]);
  }
}

const assignKeyValue = (target, key, value) => target[key] = value;
const isObject = (variable) => typeof variable === 'object'

const isFlat = (obj) => {
  for (let key in obj) {
    if (isObject(obj[key])) return false;
  }
  return true;
}

module.exports = JSONFlatten;

