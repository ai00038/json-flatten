/** Assumes that the source data is an array of flat JSON items with consistent columns */

const JSONToCSV = (flatJSONArray) => {
    const columnHeaders = Object.keys(flatJSONArray[0]);
    let csv = [ columnHeaders ];

    flatJSONArray.forEach((flatElement) => {
        csv.push(getValues(flatElement));
    });

    return csv;
}

const getValues = (flatElement) => {
    let values = [];

    for(let key in flatElement) {
        values.push(flatElement[key]);
    }

    return values;
}

module.exports = JSONToCSV;