/** The purpose of JSONFlatten is to flatten nested JSON objects into flat objects */
const JSONFlatten = require('./json-flatten');
const JSONToCSV = require('./json-to-csv');
const users = require('./users');
const fs = require('fs');

let flattenedUsers = users.map(JSONFlatten);

let usersCSV = JSONToCSV(flattenedUsers);

let usersCSVString = usersCSV.map((row) => {
    return row.join(',');
}).join('\n');

fs.writeFile('users.csv', usersCSVString, (err) => {
    if (err) { 
        console.log('Error: ', err);
        return;
    }

    console.log('File uploaded successfully');
    
});
